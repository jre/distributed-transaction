<?php
declare(strict_types = 1);

namespace Initiating;
/**
 * 事件常量
 * @author mg
 */
class EventConst
{
	/**
	 * 发起者注册事务组
	 */
	public const ADD_TX_GROUP = 0;
	
	/**
	 * 注册事务参与者
	 */
	public const REGIST_TRANSACTION = 1;
	
	/**
	 * 发起者发起precommit
	 */
	public const PRE_COMMIT = 2;
	
	/**
	 * 发起者发起docommit
	 */
	public const DO_COMMIT = 3;
	
	/**
	 * 参与者等待调用
	 */
	public const WAIT_FOR_RECEIVE_DATA = 4;
	
	/**
	 * RollBack
	 */
	public const ROLL_BACK = 5;
	
	/**
	 * 事务提交结果
	 */
	public const RESULT_PRE_COMMIT = 6;
	
	/**
	 * 真实提交
	 */
	public const REAL_COMMIT = 7;
}