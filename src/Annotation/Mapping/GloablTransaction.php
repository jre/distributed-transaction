<?php
declare(strict_types = 1);

namespace Initiating\Annotation\Mapping;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
/**
 * 全局事务处理
 * @author mg
 * @Annotation
 * @Attributes({
 *     @Attribute("isStart",type="bool"),
 *     @Attribute("timeout",type="int"),
 *     @Attribute("actorNumber",type="int")
 * })
 */
class GloablTransaction
{
	/**
	 * 是否开启事务
	 * @var string
	 */
	private $isStart = false;
	
	private $timeout = 3;
	
	/**
	 * 事务参与者数量
	 * @var integer
	 */
	private $actorNumber = 6;
	
	public function getIsStart(): bool {
		return $this->isStart;
	}
	
	public function getTimeout(): int {
		return $this->timeout;
	}
	
	public function getActorNumber(): int {
		return $this->actorNumber;
	}
	
	/**
	 * StringType constructor.
	 *
	 * @param array $values
	 */
	public function __construct(array $values)
	{
		if (isset($values['isStart'])) {
			$this->isStart = $values['isStart'];
		}
		if (isset($values['timeout'])) {
			$this->timeout = $values['timeout'];
		}
		
		if (isset($values['actorNumber'])) {
			$this->actorNumber = $values['actorNumber'];
		}
		
	}
}