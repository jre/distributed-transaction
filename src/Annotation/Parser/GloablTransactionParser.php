<?php
declare(strict_types = 1);

namespace Initiating\Annotation\Parser;

use Swoft\Annotation\Annotation\Mapping\AnnotationParser;
use Swoft\Annotation\Annotation\Parser\Parser;
use Initiating\Annotation\Mapping\GloablTransaction;
use Initiating\AnnotationData\AnnotationData;

/**
 * 全局事务处理
 * @author mg
 * @AnnotationParser(annotation=GloablTransaction::class)
 */
class GloablTransactionParser extends Parser
{
	  /**
     * Parse object
     *
     * @param int $type Class or Method or Property
     * @param GloablTransaction $annotationObject
     *
     * @return array
     * Return empty array is nothing to do!
     * When class type return [$beanName, $className, $scope, $alias] is to inject bean
     * When property type return [$propertyValue, $isRef] is to reference value
     */
    public function parse(int $type,  $annotationObject): array
    {
    	
    	// Only to parse class annotation with `@GloablTransaction`
    	if ($type != self::TYPE_METHOD) {
    		return [];
    	}
    	
    	AnnotationData::add($this->className, $annotationObject);
    	
    	return [];
    }
}