<?php declare(strict_types=1);

namespace Initiating\AnnotationData;

class AnnotationData
{
	/**
	 * @example [
	 * 	'类名' => '注解对象'
	 * ]
	 * @var array
	 */
	private static $annotationData = [];
	
	public static function add(string $className, object $annotationObj): void
	{
		static::$annotationData[$className] = $annotationObj;
	}
	
	public static function get(string $className): object
	{
		return static::$annotationData[$className];
	}
	
	public function destory(string $className): void
	{
		unset(static::$annotationData[$className]);
	}
}