<?php 

declare ( strict_types = 1 )
;

namespace Initiating\Contract;


/**
 * 事务资源
 * @author mg
 *
 */
interface ITxTransaction{

	function begin() :bool;

	function commit() :bool;

	function rollBack() :bool;
}