<?php declare(strict_types=1);

namespace Initiating;

use Swoole\Server;
use Initiating\Pool\RedisPool;

/**
 * 测试用【不支持注解】
 * @author mg
 *
 */
class App
{
	/**
	 * 
	 * @var Server
	 */
	public static $server;
	
	/**
	 * 
	 * @var RedisPool
	 */
	public static $redisPool;
	
	/**
	 * 
	 * @var AMQPConnection
	 */
	public static $rabbitMQ;
}