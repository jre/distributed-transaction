# distributed transaction

#### 介绍
PHP 分布式事务客户端

#### 软件架构
软件架构说明



#### 使用说明

1.  此插件 基于swoole开发
2.  使用的是分布式事务中的AT模式，对业务无侵入，如同本地事务一样方便简单
3.  提交是协程等待异步提交的

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


