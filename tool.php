<?php
declare(strict_types = 1);

function dump($data)
{

    $trance = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

    print_r(PHP_EOL.'-------------------------------------'.PHP_EOL);

    print_r($trance[0]['file'].'  <=====>  '.$trance[0]['line'].PHP_EOL);
    print_r($data);


    print_r(PHP_EOL.'-------------------------------------'.PHP_EOL);

}